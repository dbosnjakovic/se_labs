from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse

from .forms import ImageForm, CommentForm
from .models import Image, Comment, Vote, Like


# Create your views here.


def index(request):
    images = Image.objects.order_by("-pub_date")
    votes = [image.vote_by(request.user) for image in images]

    if request.user.is_superuser:
        approvals = [image.comment_set.filter(image=image).count() for image in images]
    else:
        approvals = [image.comment_set.filter(image=image, approved=True).count() for image in images]

    context = {"images_with_votes_and_approvals": zip(images, votes, approvals)}
    return render(request, "app/index.html", context)


def detail(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    comments = Comment.objects.filter(image=image_id)
    likes = [comment_like.like_by(request.user) for comment_like in comments]

    context = {
        "image": image,
        "vote": image.vote_by(request.user),
        "comments_with_likes": zip(image.comment_set.all(), likes),
        "form": CommentForm(),
    }
    return render(request, "app/detail.html", context)


def comment(request, image_id):
    if request.method == "POST" and request.user.is_authenticated:
        image = get_object_or_404(Image, pk=image_id)
        form = CommentForm(request.POST)
        if form.is_valid():
            image_comment = form.save(commit=False)
            image_comment.image = image
            image_comment.user = request.user

            if request.user.is_superuser:
                image_comment.approved = True

            image_comment.save()
            return HttpResponseRedirect(reverse("app:detail", args=(image_comment.image.id,)))
        else:
            return render(
                request,
                "app/detail.html",
                {
                    "image": image,
                    "comments": image.comment_set.all(),
                    "form": form,
                },
            )
    else:
        return HttpResponseRedirect(reverse("app:detail", args=(image_id,)))


def comment_approve(request, image_id, comment_id):
    if request.method == "POST" and request.user.is_superuser:
        comment_to_approve = get_object_or_404(Comment, pk=comment_id, image_id=image_id)
        comment_to_approve.approved = True

        try:
            comment_to_approve.full_clean()
            comment_to_approve.save()
        except:
            return None
        else:
            return HttpResponseRedirect(reverse("app:detail", args=(image_id,)))


def create_image(request):
    if request.method == "POST" and request.user.is_authenticated:
        form = ImageForm(request.POST)
        if form.is_valid():
            saved_image = form.save(commit=False)
            saved_image.user = request.user
            saved_image.save()

            return HttpResponseRedirect(reverse("app:detail", args=(saved_image.id,)))
    else:
        form = ImageForm()

    context = {"form": form, "action": "create"}
    return render(request, "app/create_image.html", context)


def update_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if request.user.is_authenticated and (request.user.id == image.user_id or request.user.is_superuser):
        if request.method == "POST":
            form = ImageForm(request.POST, instance=image)
            if form.is_valid():
                saved_image = form.save()
                return HttpResponseRedirect(reverse("app:detail", args=(saved_image.id,)))
        else:
            form = ImageForm(instance=image)

        context = {"form": form, "action": "update"}
        return render(request, "app/create_image.html", context)


def delete_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if (
        request.method == "POST"
        and request.user.is_authenticated
        and (request.user.id == image.user_id or request.user.is_superuser)
    ):
        image.delete()
        return HttpResponseRedirect(reverse("app:index"))


def vote(request, image_id, status):
    image = get_object_or_404(Image, pk=image_id)
    user_vote = Vote.objects.filter(user=request.user, image=image).first()

    if user_vote:
        if user_vote.upvote == status:
            user_vote.delete()
            return None
        else:
            user_vote.upvote = status
    else:
        user_vote = Vote(user=request.user, image=image, upvote=status)

    try:
        user_vote.full_clean()
        user_vote.save()
    except:
        return None
    else:
        return user_vote


def upvote(request, image_id):
    if request.method == "POST" and request.user.is_authenticated:
        vote(request, image_id, True)
    return HttpResponseRedirect(reverse("app:detail", args=(image_id,)))


def downvote(request, image_id):
    if request.method == "POST" and request.user.is_authenticated:
        vote(request, image_id, False)
    return HttpResponseRedirect(reverse("app:detail", args=(image_id,)))


def like(request, comment_id):
    if request.method == "POST" and request.user.is_authenticated:
        comment_like = get_object_or_404(Comment, pk=comment_id)
        user_like = Like.objects.filter(user=request.user, comment=comment_like).first()

        if user_like:
            user_like.like = not user_like.like
        else:
            user_like = Like(user=request.user, comment=comment_like, like=True)

        try:
            user_like.full_clean()
            user_like.save()
        except:
            return None
        else:
            return HttpResponseRedirect(reverse("app:detail", args=(comment_like.image.id,)))
