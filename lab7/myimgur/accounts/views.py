from django.shortcuts import get_object_or_404, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from app.models import Comment, Image

# Create your views here.


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"


def accounts(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    images = Image.objects.filter(user=user_id)
    comments = Comment.objects.filter(user=user_id)

    context = {"user": user, "images": images, "comments": comments}
    return render(request, "app/profile.html", context)
