### prebacivanje u lokalni git repo
```
cd git/se_labs
```

### stvaranje lab5 direktorija
```
mkdir lab5
```

### kreiranje virtual environmenta
```
python -m venv django
```

### aktivacija venv-a
```
source django/bin/activate
```

### azuriranje pip alata
```
python -m pip install --upgrade pip
 ```

### provjera django verzije
```
django-admin --version
```

### kreiranje django projekta
```
django-admin startproject myimgur
```

### pokretanje servera
```
python manage.py runserver
```

### dodavanje lab5/django venv u .gitignore
```
vim .gitignore
```

```
git add lab5
```

```
git commit -m "Add myimgur initial project to lab5"
```

```
git add .gitignore
```

### dodavanje na posljednji commit
```
git commit --amend
```

```
git status
```

```
git log
```

### ispisuje verzije paketa na ekran
```
pip freeze
```

```
cd lab5
```

### ispisuje verzije paketa u datoteku
```
pip freeze > requirements.txt
```

### instalira pakete iz datoteke
```
pip install -r requirements.txt
```
