
# Najbolji Git tutorijal

test

```
git status
```
#### Naredba prikazuje lokalne promjene koje trenutno nisu praćene.

```
git add
```
#### Naredba služi za dodavanje promjena koje smo napravili ili dodavanje novih datoteka za praćenje.

```
git log
```
#### Naredba prikazuje sve lokalne promjene (commitove), autora te datum i vrijeme promjene.

```
git commit
```
#### Naredba služi za kreiranje commita koji prikazuje promjene koje smo napravili.

```
git diff
```
#### Naredba prikazuje razlike između commita, tj. linije koje su promijenjene.

```
git push
```
#### Naredba služi za slanje lokalnih promjena na repositorij.

```
git config
```
#### Naredba služi za izmjenu postavki koje koristi git, kao što su username i e-mail.

```
git pull
```
#### Naredba služi za dohvaćanje promjena sa udaljenog repositorija na lokalni.
