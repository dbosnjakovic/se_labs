from .models import Image, Comment, Order
from django.forms import ModelForm, widgets
from bootstrap_datepicker_plus import DateTimePickerInput
class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields = ['title', 'url', 'description', 'pub_date']
        widgets = {
            'pub_date': DateTimePickerInput()
        }
class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']

class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = [ 'fname', 'lname', "address", "comment" ]
