from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Image, Comment

# Create your views here.

def index(request):
    images = Image.objects.order_by('-pub_date')
    context = { 'images': images}
    return render(request, 'app/index.html', context)

def detail(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
    context = {'image': image,
               'comments': image.comment_set.all(),
              }
    return render(request, 'app/detail.html', context)

def comment(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    try:
        comment = image.comment_set.create(
                   author=request.POST['author'],
                   text=request.POST['comment'],
                )
    except (KeyError, Comment.DoesNotExist):
        # Redisplay the comment posting form.
        return render(request, 'app/detail.html', {
            'image': image,
            'error_message': "Posting failed!",
        })
    else:
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))


def upvote(request, image_id):
	image = get_object_or_404(Image, pk=image_id)
	if request.method == 'POST':
		image.upvotes += 1
		image.save()
	return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))


def downvote(request, image_id):
	image = get_object_or_404(Image, pk=image_id)
	if request.method == 'POST':
		image.downvotes += 1
		image.save()
	return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))

def about(request):
	return render(request, 'app/about.html', {})

def new(request):
	if request.method == 'GET':
		return render(request, 'app/new.html', {})
	else:
		image = Image.objects.create(
			title=request.POST['title'],
			url=request.POST['img_url'],
			pub_date=request.POST['date'],
			description=request.POST['description']
		)
		return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))
