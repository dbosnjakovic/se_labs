# Kreiranje virtual environmenta

```
python -m venv NAZIV_ENVA
```

# Aktivacija venva

```
source NAZIV_ENVA/bin/activate
```

# Deaktivacija enva

```
source ~/.zshrc
```
