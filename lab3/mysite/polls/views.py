# Create your views here.

from django.http import HttpResponse
from .models import Question
from django.shortcuts import get_object_or_404, render


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)


def welcome(request):
    return HttpResponse("<h1>Welcome!</h1>")


def surname(request):
    return HttpResponse("<h1>Bošnjaković</h1>")


def test(request):
    txt = '<h1>Test</h1>'
    questions = Question.objects.all()
    for q in questions:
        qtxt = "<p>" + str(q) + " - " + str(q.pub_date) + "</p>"
        txt = txt + qtxt

    return HttpResponse(txt)


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)
