#!/usr/bin/env python

choices = {"rock": 1, "paper": 2, "scissors": 3, "lizard": 4, "spock": 5}
players = ["draw", "player1", "player2", "player3"]


def winner_2p(c1, c2):
    if c1 == c2:
        return 0
    elif c1 == 1:
        if c2 == 2:
            return 2
        elif c2 == 3:
            return 1
        elif c2 == 4:
            return 1
        elif c2 == 5:
            return 2
    elif c1 == 2:
        if c2 == 1:
            return 1
        elif c2 == 3:
            return 2
        elif c2 == 4:
            return 2
        elif c2 == 5:
            return 1
    elif c1 == 3:
        if c2 == 1:
            return 2
        elif c2 == 2:
            return 1
        elif c2 == 4:
            return 1
        elif c2 == 5:
            return 2
    elif c1 == 4:
        if c2 == 1:
            return 2
        elif c2 == 2:
            return 1
        elif c2 == 3:
            return 2
        elif c2 == 5:
            return 1
    elif c1 == 5:
        if c2 == 1:
            return 1
        elif c2 == 2:
            return 2
        elif c2 == 3:
            return 1
        elif c2 == 4:
            return 2
    return False


def winner(c1, c2, c3):
    if c1 == c2 == c3:
        return 0
    round_winner = winner_2p(c1, c2)

    if round_winner == 1:
        winner2 = winner_2p(c1, c3)
        if winner2 == 0:
            return 0
        elif winner2 == 1:
            return 1
        else:
            return 3
    elif round_winner == 2:
        winner2 = winner_2p(c2, c3)
        if winner2 == 0:
            return 0
        elif winner2 == 1:
            return 2
        else:
            return 3

    if round_winner == 0 and c1 == c2 and winner_2p(c1, c3) == 2:
        return 3
    else:
        return 0


def is_valid_input(i):
    valid_choices = list(choices.keys())
    return i in valid_choices


def check_inputs(p1, p2, p3):
    return is_valid_input(p1) and is_valid_input(p2) and is_valid_input(p3)


def main():
    while True:
        player1 = input("Player 1, pick " + str(list(choices.keys())) + ": ")
        player2 = input("Player 2, pick " + str(list(choices.keys())) + ": ")
        player3 = input("Player 3, pick " + str(list(choices.keys())) + ": ")

        if check_inputs(player1, player2, player3):
            result = winner(choices[player1], choices[player2], choices[player3])
        else:
            print("Wrong choice!")
            continue

        print("Winner is " + players[result])

        if input("Continue [Y/n]: ") == "n":
            break


def test_inputs(i, expected):
    result = is_valid_input(i)
    try:
        assert result == expected
        print("PASS")
    except AssertionError:
        print("Expected:", expected)
        print("Result:", result)
        raise


def run_test_2p(p1, p2, expected):
    result = winner_2p(p1, p2)
    try:
        assert result == expected
    except AssertionError:
        print("expected:", expected)
        print("result:", result)
        raise
    print("PASS")

def run_test(p1, p2, p3, expected):
    result = winner(p1, p2, p3)
    try:
        assert result == expected
    except AssertionError:
        print("expected:", expected)
        print("result:", result)
        raise
    print("PASS")


def unit_tests():
    test_inputs("rock", True)
    test_inputs("paper", True)
    test_inputs("scissors", True)
    test_inputs("pero", False)
    test_inputs("", False)
    test_inputs(7, False)
    test_inputs(-3.14, False)
    test_inputs("lizard", True)
    test_inputs("spock", True)

    run_test_2p(1, 1, 0)
    run_test_2p(1, 2, 2)
    run_test_2p(1, 3, 1)
    run_test_2p(1, 4, 1)
    run_test_2p(1, 5, 2)

    run_test_2p(2, 1, 1)
    run_test_2p(2, 2, 0)
    run_test_2p(2, 3, 2)
    run_test_2p(2, 4, 2)
    run_test_2p(2, 5, 1)

    run_test_2p(3, 1, 2)
    run_test_2p(3, 2, 1)
    run_test_2p(3, 3, 0)
    run_test_2p(3, 4, 1)
    run_test_2p(3, 5, 2)

    run_test_2p(4, 1, 2)
    run_test_2p(4, 2, 1)
    run_test_2p(4, 3, 2)
    run_test_2p(4, 4, 0)
    run_test_2p(4, 5, 1)

    run_test_2p(5, 1, 1)
    run_test_2p(5, 2, 2)
    run_test_2p(5, 3, 1)
    run_test_2p(5, 4, 2)
    run_test_2p(5, 5, 0)

    run_test_2p(6, 1, False)
    run_test_2p(7, 2, False)
    run_test_2p(8, 3, False)
    run_test_2p(1, "", False)
    run_test_2p(2, 44, False)
    run_test_2p("", "pero", False)

    run_test(1, 1, 1, 0)
    run_test(2, 2, 2, 0)
    run_test(3, 3, 3, 0)
    run_test(4, 4, 4, 0)
    run_test(5, 5, 5, 0)

    run_test(1, 1, 2, 3)
    run_test(1, 2, 1, 2)
    run_test(2, 1, 1, 1)

    run_test(1, 1, 3, 0)
    run_test(1, 3, 1, 0)
    run_test(3, 1, 1, 0)

    run_test(1, 1, 4, 0)
    run_test(1, 4, 1, 0)
    run_test(4, 1, 1, 0)

    run_test(1, 1, 5, 3)
    run_test(1, 5, 1, 2)
    run_test(5, 1, 1, 1)


if __name__ == "__main__":
    unit_tests()
    main()
