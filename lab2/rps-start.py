#!/usr/bin/env python

def winner(player1, player2):
    if not check_inputs(player1, player2):
        return False
    if player1 == player2:
        return("draw")
    elif player1 == "rock":
        if player2 == "paper":
            return("player2")
        elif player2 == "scissors":
            return("player1")
    elif player1 == "paper":
        if player2 == "rock":
            return("player1")
        elif player2 == "scissors":
            return("player2")
    elif player1 == "scissors":
        if player2 == "rock":
            return("player2")
        elif player2 == "paper":
            return("player1")
    return False;

def is_valid_input(i):
    choices = ["rock", "paper", "scissors"]
    return i in choices

def check_inputs(p1, p2):
    return is_valid_input(p1) and is_valid_input(p2)

def main():
    while True:
        player1 = input("rock-paper-scissors: ")
        player2 = input("rock-paper-scissors: ")
        if check_inputs(player1, player2):
            result = winner(player1, player2)
        else:
            print("neispravan izbor, probaj ponovno")
            continue

        print("pobijedio je", result)

        nastavi = input("nastavi yes/no")
        if nastavi == "no":
            break

def run_test(player1, player2, expected):
    print(player1, "vs.", player2)
    result = winner(player1, player2)
    try:
        assert result == expected
    except AssertionError:
        print("expected:", expected)
        print("result:", result)
        raise
    print("PASS")

def test_inputs(i, expected):
    result = is_valid_input(i)
    try:
        assert result == expected
        print("PASS")
    except AssertionError:
        print("expected:", expected)
        print("result:", result)
        raise

def unit_tests():
    test_inputs("rock", True)
    test_inputs("paper", True)
    test_inputs("scissors", True)
    test_inputs("pero", False)
    test_inputs("", False)
    test_inputs(8, False)
    run_test("rock", "paper", "player2")
    run_test("rock", "scissors", "player1")
    run_test("paper", "rock", "player1")
    run_test("paper", "paper", "draw")
    run_test("paper", "scissors", "player2")
    run_test("scissors", "rock", "player2")
    run_test("scissors", "paper", "player1")
    run_test("scissors", "scissors", "draw")
    run_test("pero", "djuro", False)
    run_test("", "scissors", False)
    run_test("pero", "pero", False)

if __name__ == "__main__":
    #main()
    unit_tests()

