import random

random_number = random.randint(1, 9)
guesses = 0

while True:
    number = input("Guess number: ")
    if number == "exit":
        print("You quit!")
        exit(1)
    else:
        number = int(number)

    guesses += 1
    if number < random_number:
        print("Too low! Missed by " + str(random_number - number) + ".")
    elif number > random_number:
        print("Too high! Missed by " + str(number - random_number) + ".")
    else:
        print("Exactly!")
        print("It only took you " + str(guesses) + " guesses.")
        exit(0)
