#!/usr/bin/env python

# Ask the user for a string and print out whether this string is a palindrome or not.

string = input("Enter a string: ")

if string[::] == string[::-1]:
    print("Entered string is a palindrome.")
else:
    print("Entered string is not a palindrome.")
