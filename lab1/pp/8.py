#!/usr/bin/env python

# Make a two-player Rock-Paper-Scissors game.

rps = ["rock", "paper", "scissors"]

play_again = True
while play_again is True:
    player1 = input("Player 1, pick rock, paper or scissors: ")
    player2 = input("Player 2, pick rock, paper or scissors: ")

    if player1 not in rps or player2 not in rps:
        print("Wrong choice!")
    else:
        if player1 == player2:
            print("It's a draw!")
        elif player1 == "paper":
            if player2 == "rock":
                print("Paper wins!")
            else:
                print("Scissors win!")
        elif player1 == "scissors":
            if player2 == "paper":
                print("Scissors win!")
            else:
                print("Rock wins!")
        elif player1 == "rock":
            if player2 == "scissors":
                print("Rock wins!")
            else:
                print("Paper wins!")

    repeat = input("Do you want to play again [y/N]? ")
    if repeat.lower() == "y":
        play_again = True
    else:
        play_again = False
